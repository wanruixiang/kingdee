# Program List 2-1
# K-nearest neighbor algorithm

from numpy import *
import operator

def creat_data_set():
    group = array([[1.0,1.1],[1.0,1.0],[0,0],[0,0.1]])
    labels = ['A','A','B','B']
    return group, labels

def classify0(in_x, data_set, labels, k):
    data_set_size = data_set.shape[0]
    diff_mat = tile(in_x, (data_set_size,1)) - data_set
    sq_diff_mat = diff_mat**2
    sq_distances = sq_diff_mat.sum(axis=1)
    distances = sq_distances**0.5
    sorted_distance_indices = distances.argsort()
    class_count={}
    for i in range(k):
        vote_i_label = labels[sorted_distance_indices[i]]
        class_count[vote_i_label] = class_count.get(vote_i_label,0) + 1
    sorted_class_count = sorted(class_count.items(),
                              key = operator.itemgetter(1),
                              reverse = True)
    return sorted_class_count[0][0]

'''Test code
group, labels = creat_data_set()
print(classify0([0,0], group, labels, 3))
'''

# Program List 2-2
# File to matrix & 3D scatter plot

def file_to_matrix(filename):
    fr = open(filename)
    array_of_lines = fr.readlines()
    number_of_lines = len(array_of_lines)
    return_mat = zeros((number_of_lines, 3))
    class_label_vector = []
    index = 0
    for line in array_of_lines:
        line = line.strip()
        list_from_line = line.split('\t')
        return_mat[index,:] = list_from_line[0:3]
        class_label_vector.append(int(list_from_line[-1]))
        index += 1
    return return_mat, class_label_vector

dating_data_mat, dating_labels = file_to_matrix('dating_test_set_2.txt')

'''Test code
from mpl_toolkits import mplot3d
import matplotlib
import matplotlib.pyplot as plt
fig = plt.figure()
#ax = fig.add_subplot(111)
ax = plt.axes(projection='3d')
#ax.scatter(dating_data_mat[:,1], dating_data_mat[:,2], 15.0*array(dating_labels),  15.0*array(dating_labels))
ax.scatter3D(dating_data_mat[:,0],
             dating_data_mat[:,1],
             dating_data_mat[:,2],
             zdir='z',
             c=dating_labels,
             depthshade=True,
             cmap='viridis',
             linewidth=0.1)
plt.show()
'''

# Program List 2-3
# Normalized eigenvalues

def auto_norm(data_set):
    min_vals = data_set.min(0)
    max_vals = data_set.max(0)
    ranges = max_vals - min_vals
    norm_data_set = zeros(shape(data_set))
    m = data_set.shape[0]
    norm_data_set = data_set - tile(min_vals, (m,1))
    norm_data_set = norm_data_set/tile(ranges, (m,1))
    return norm_data_set, ranges, min_vals

# Program List 2-4
# Dating class test

def dating_class_test():
    ho_ratio = 0.1
    dating_data_mat, dating_labels = file_to_matrix('dating_test_set_2.txt')
    norm_mat, ranges, min_vals = auto_norm(dating_data_mat)
    m = norm_mat.shape[0]
    num_test_vecs = int(m*ho_ratio)
    error_count = 0.0
    for i in range(num_test_vecs):
        classifier_result = classify0(norm_mat[i,:], norm_mat[num_test_vecs:m,:],
                                      dating_labels[num_test_vecs:m], 3)
        print("the classifier came back with: %d, the real anwser is %d"
              % (classifier_result, dating_labels[i]))
        if (classifier_result != dating_labels[i]): error_count += 1.0
    print("the total error rate is : %f%%" % (error_count/float(num_test_vecs) * 100))

'''Test code
dating_class_test()
'''

# Program List 2-5
# Classify person

def classify_person():
    result_list = ['not at all', 'in small doses', 'in large doses']
    frequent_flier_miles = float(input("frequent flier miles earned per year?\n"))
    video_games = float(input("percentage of time spent playing video games?\n"))
    ice_cream = float(input("liters of ice cream consumed per year?\n"))
    dating_data_mat, dating_labels = file_to_matrix('dating_test_set_2.txt')
    norm_mat, ranges, min_vals = auto_norm(dating_data_mat)
    in_array = array([frequent_flier_miles, video_games, ice_cream, ])
    classifier_result = classify0((in_array - min_vals)/ranges,
                                  norm_mat, dating_labels, 3)
    print("You will probably like this person %s."
          % result_list[classifier_result - 1])

'''Test code
classify_person()
'''

# Program List 2-6
# Handwriting class test

def img_to_vector(filename):
    return_vect = zeros((1,1024))
    fr = open(filename)
    for i in range(32):
        line_str = fr.readline()
        for j in range(32):
            return_vect[0,32*i+j] = int(line_str[j])
    return return_vect

from os import listdir

def handwriting_class_test():
    hw_labels = []
    training_file_list = listdir('training_digits')
    m = len(training_file_list)
    training_mat = zeros((m,1024))
    for i in range(m):
        file_name_str = training_file_list[i]
        file_str = file_name_str.split('.')[0]
        class_num_str = int(file_str.split('_')[0])
        hw_labels.append(class_num_str)
        training_mat[i,:] = img_to_vector('training_digits/%s' % file_name_str)
    test_file_list = listdir('test_digits')
    error_count = 0.0
    m_test = len(test_file_list)
    for i in range(m_test):
        file_name_str = test_file_list[i]
        file_str = file_name_str.split('.')[0]
        class_num_str = int(file_str.split('_')[0])
        vector_under_test = img_to_vector('test_digits/%s' % file_name_str)
        classifier_result = classify0(vector_under_test,
                                      training_mat, hw_labels, 3)
        print("the classifier came back with: %d, the real anwser is %d"
              % (classifier_result, class_num_str))
        if (classifier_result != class_num_str): error_count += 1.0
    print("\nthe total number of error is: %d" % error_count)
    print("\nthe total error rate is: %f%%" % (error_count/float(m_test)*100))

'''Test code
handwriting_class_test()
'''
