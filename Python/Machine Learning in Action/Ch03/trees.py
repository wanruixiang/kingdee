# Program List 3-1
# Calculate Shannon Entropy

from math import log

def calc_shannon_ent(data_set):
    num_entries = len(data_set)
    label_counts = {}
    for feat_vec in data_set:
        current_label = feat_vec[-1]
        if current_label not in label_counts.keys():
            label_counts[current_label] = 0
        label_counts[current_label] += 1
    shannon_ent = 0.0
    for key in label_counts:
        prob = float(label_counts[key])/num_entries
        shannon_ent -= prob * log(prob,2)
    return shannon_ent

def create_data_set():
    data_set = [[1, 1, 1, 1],
                [1, 1, 0, 1],
                [1, 0, 1, 1],
                [1, 0, 0, 0],
                [0, 1, 1, 1],
                [0, 1, 0, 0],
                [0, 0, 1, 1],
                [0, 0, 0, 0]]
    labels = ['A','B','C'] # (A and B)or C
    return data_set, labels

'''Test code
data_set, labels = create_data_set()
'''

# Program List 3-2
# Split data set

def split_data_set(data_set, axis, value):
    return_data_set = []
    for feat_vec in data_set:
        if feat_vec[axis] == value:
            reduced_feat_vec = feat_vec[:axis]
            reduced_feat_vec.extend(feat_vec[axis+1:])
            return_data_set.append(reduced_feat_vec)
    return return_data_set

'''Test code
split_data_set(data_set,0,1)
split_data_set(data_set,0,0)
'''

# Program List 3-3
# Choose the best feature to split

def choose_best_feature_to_split(data_set):
    num_features = len(data_set[0]) - 1
    base_entropy = calc_shannon_ent(data_set)
    best_info_gain = 0.0; best_feature = -1
    for i in range(num_features):
        feat_list = [example[i] for example in data_set]
        unique_vals = set(feat_list)
        new_entropy = 0.0
        for value in unique_vals:
            sub_data_set = split_data_set(data_set, i, value)
            prob = len(sub_data_set)/float(len(data_set))
            new_entropy += prob * calc_shannon_ent(sub_data_set)     
        info_gain = base_entropy - new_entropy
        if (info_gain > best_info_gain):
            best_info_gain = info_gain 
            best_feature = i
    return best_feature

'''Test code
print(choose_best_feature_to_split(create_data_set()[0]))
'''

import operator

def majority_cnt(class_list):
    class_count={}
    for vote in class_list:
        if vote not in class_count.keys(): class_count[vote] = 0
        class_count[vote] += 1
    sorted_class_count = sorted(class_count.items(), key=operator.itemgetter(1), reverse=True)
    return sorted_class_count[0][0]

# Program List 3-4
# Creat tree

def create_tree(data_set, labels):
    class_list = [example[-1] for example in data_set]
    if class_list.count(class_list[0]) == len(class_list): 
        return class_list[0]
    if len(data_set[0]) == 1:
        return majority_cnt(class_list)
    best_feat = choose_best_feature_to_split(data_set)
    best_feat_label = labels[best_feat]
    my_tree = {best_feat_label:{}}
    del(labels[best_feat])
    feat_values = [example[best_feat] for example in data_set]
    unique_vals = set(feat_values)
    for value in unique_vals:
        sub_labels = labels[:]
        my_tree[best_feat_label][value] = create_tree(split_data_set(data_set, best_feat, value),sub_labels)
    return my_tree     

'''Test code
data_set, labels = create_data_set()
my_tree = create_tree(data_set, labels)
print(my_tree)
'''

# Program List 3-8
# Classify tree

def classify(input_tree,feat_labels,test_vec):
    first_str = list(input_tree.keys())[0]
    second_dict = input_tree[first_str]
    feat_index = feat_labels.index(first_str)
    key = test_vec[feat_index]
    value_of_feat = second_dict[key]
    if isinstance(value_of_feat, dict): 
        class_label = classify(value_of_feat, feat_labels, test_vec)
    else: class_label = value_of_feat
    return class_label

# Program List 3-9
# Store tree

def store_tree(input_tree, filename):
    import pickle
    fw = open(filename,'w')
    pickle.dump(input_tree,fw)
    fw.close()
    
def grab_tree(filename):
    import pickle
    fr = open(filename)
    return pickle.load(fr)

'''Test code'''
fr = open('lenses.txt')
lenses = [inst.strip().split('\t') for inst in fr.readlines()]
lenses_labels = ['age', 'prescript', 'astigmatic', 'tear_rate']
lenses_tree = create_tree(lenses, lenses_labels)
import tree_plotter
tree_plotter.create_plot(lenses_tree)
